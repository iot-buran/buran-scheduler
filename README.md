# buran-scheduler

Scheduler module for buran

# Docker Run

0. Prerequisits

Build image buran-dbmodule

1. Build

docker build -t buran-scheduler .

2. Run

docker run -d -it -e DOWNLOAD_PATH={SPECIFY_DOWNLOAD_PATH} -e EVENT_SOCKET_URL={SPECIFY_SOCKET_UEL}-e DB_URL={SPECIFY_MONGO_URL} buran-scheduler

Example:

docker run -d -it -e EVENT_SOCKET_URL=http://192.168.104.199:1111 -e DB_URL=mongodb://192.168.104.199:27017 buran-scheduler