const EVENT_SOCKET_URL = process.env.EVENT_SOCKET_URL || 'http://localhost:3333'

let faye = require('faye');
let eventSocket = new faye.Client(EVENT_SOCKET_URL);
let db = require('dbmodule') 
let schedule = require('node-schedule');

let scheduledJobs = {}

db.connectToDB().then(client => {
    db.getFilteredCampaigns({ $or: [ { status: "New" }, { status: "Scheduled" } ] } )
        .then(campaigns => campaigns.forEach(scheduleNewJobs))
});

let scheduleNewJobs = async campaign => {
    let campaignObj;
    let campaignId;
    if (typeof campaign === "string") {
        campaignId = campaign
        campaignObj = await db.getCampaignById(campaignId)
    } else {
        campaignId = campaign._id.toString()
        campaignObj = campaign
    }

    if (scheduledJobs[campaignId]) {
        scheduledJobs[campaignId].forEach(job => job.cancel())
        delete scheduledJobs[campaignId]
    }

    console.log(`Creating jobs for ${campaignId}`)

    let devicesReach = campaignObj.devices ? campaignObj.devices.length : 0
    let devicesPerHour = campaignObj.throttling ? campaignObj.throttling : devicesReach
    let iterations = devicesPerHour > 0 ? devicesReach / devicesPerHour : 0
    let remainder = devicesPerHour > 0 ? devicesReach % devicesPerHour : 0
    if (remainder > 0) {
        iterations++
    }

    if (iterations == 0) {
        db.upsertCampaign(campaignId, {status: "Nothing to Do"})
        return
    }

    let date = new Date(campaignObj.startTime);
    scheduledJobs[campaignId] = []
    for (let i = 0; i < iterations; i++) {
        date.setHours(date.getHours() + (i == 0 ? 0 : 1))
        let j = schedule.scheduleJob(date, runJob(campaignId, devicesPerHour));    
        if (j) {
            scheduledJobs[campaignId].push(j)
        }
    }
    scheduleCleanUpJob(date, campaignId, devicesPerHour)

    if (scheduledJobs[campaignId] && scheduledJobs[campaignId].length > 0) {
        db.upsertCampaign(campaignId, {status: "Scheduled"})
    } else {
        db.upsertCampaign(campaignId, {status: "Skipped"})  
    }
}

let scheduleCleanUpJob = (date, campaignId, devicesPerHour) => {
    date.setHours(date.getHours() + 1)
    let j = schedule.scheduleJob(date, runCleanUpJob(campaignId, devicesPerHour, date));    
    if (j) {
        scheduledJobs[campaignId].push(j)
    }
}

let runJob = (campaignId, numberOfDevices) => async () => {
    runJobGeneric(campaignId, numberOfDevices, "notscheduled")
}

let runCleanUpJob = (campaignId, numberOfDevices, date) => async () => {
    let campaign = await db.getCampaignById(campaignId)
    if (!campaign.status.startsWith("Finished")) {
        runJobGeneric(campaignId, numberOfDevices, "scheduled")
        scheduleCleanUpJob(date, campaignId, numberOfDevices)
    } else {
        scheduledJobs[campaignId].shift()
    }
}

let numberOfFinishedDevices = campaign => {
    if (campaign.devices) {
        return campaign.devices.filter(dev =>  dev.status == "updatedsuccessful" || dev.status == "updatedfailed" || dev.status == "updatenotneeded").length
    } else {
        return 0
    }
}

let runJobGeneric = async (campaignId, numberOfDevices, deviceTargetStatus) => {
    let campaign = await db.getCampaignById(campaignId)
    db.upsertCampaign(campaignId, {status: `Running ${numberOfFinishedDevices(campaign)}/${campaign.devices ? campaign.devices.length : 0}`})

    if (campaign.devices) {
        let count = 0
        for (let i = 0; i < campaign.devices.length; i++) {
            if (campaign.devices[i].status == "scheduled") {
                eventSocket.publish(`/unschedule-device-${device.type}`, {deviceId: device._id})               
            }
            if (count < numberOfDevices && campaign.devices[i].status == deviceTargetStatus) {
                count++
                let device = await db.getDeviceById(campaign.devices[i]["id"])
                eventSocket.publish(`/update-device-${device.type}`, {deviceId: device._id, firmwareUrl: campaign.firmwareUrl, campaignId: campaignId, newVersion:campaign.newVersion})
                campaign.devices[i].status = "scheduled"
                db.upsertDevice(device._id, {stat: "Scheduled for Update"})           
            }
        }
        db.upsertCampaign(campaignId, {devices: campaign.devices})
    }
    scheduledJobs[campaignId].shift()
}

let deleteExistingJobs = campaignId => {
    console.log(`Deleting jobs for ${campaignId}`)
    if (scheduledJobs[campaignId]) {
        scheduledJobs[campaignId].forEach(job => job.cancel())
        delete scheduledJobs[campaignId]
    }
}

let onDeviceUpdateFinished = async (deviceId, campaignId, deviceStatus) => {
    let campaign = await db.getCampaignById(campaignId)

    if (campaign.devices) {
        db.upsertCampaignDeviceStatus(campaignId, deviceId, deviceStatus)
        for (let i = 0; i < campaign.devices.length; i++) {
            if (campaign.devices[i].id == deviceId) {
                campaign.devices[i].status = deviceStatus
            }
        }
        if (campaign.devices.every(dev => dev.status == "updatedsuccessful" || dev.status == "updatenotneeded")) {
            db.upsertCampaign(campaignId, {status: "Finished"})
        }
        else if (campaign.devices.every(dev => dev.status == "updatedsuccessful" || dev.status == "updatedfailed")) {
            db.upsertCampaign(campaignId, {status: "Finished with errors"})
        } else {
            db.upsertCampaign(campaignId, {status: `Running ${numberOfFinishedDevices(campaign)}/${campaign.devices ? campaign.devices.length : 0}`})
        }
    } else {
        db.upsertCampaign(campaignId, {status: "Finished"})
    }
}

let onSuccessfulDeviceUpdate = (deviceId, campaignId) => {
    onDeviceUpdateFinished(deviceId, campaignId, "updatedsuccessful")
}

let onNotNeededDeviceUpdate = (deviceId, campaignId) => {
    onDeviceUpdateFinished(deviceId, campaignId, "updatenotneeded")
}

let onFailedDeviceUpdate = (deviceId, campaignId) => {
    onDeviceUpdateFinished(deviceId, campaignId, "updatedfailed")
}

eventSocket.subscribe("/new-campaign", data => {
    scheduleNewJobs(data.id)
});
eventSocket.subscribe("/delete-campaign", data => {
    deleteExistingJobs(data.id)
});
eventSocket.subscribe("/device-updatesuccess", data => {
    onSuccessfulDeviceUpdate(data.deviceId, data.campaignId)
})
eventSocket.subscribe("/device-updatenotneeded", data => {
    onNotNeededDeviceUpdate(data.deviceId, data.campaignId)
})
eventSocket.subscribe("/device-updatedfailed", data => {
    onFailedDeviceUpdate(data.deviceId, data.campaignId)
})